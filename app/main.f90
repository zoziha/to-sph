!! to-sph 主要是 ti-sph 的前处理程序，可用于初始化粒子

!> author: 左志华
!> date: 2022-05-02
!>
!> sph / ti-sph pre-post-processing program <br>
!> sph 前后处理程序 -- to-sph
program main

    use seakeeping_time, only: tic, toc, now
    use sph_logger_m, only: stdlog, set_log_file
    use sph_region_type, only: region_t
    use sph, only: cmd_pre, cmd_post
    use seakeeping_filesystem, only: is_exist, operator(.join.), mkdir
    use sph_terminal, only: ts => to_sph_terminal, error, set_terminal
    use sph_command_line, only: get_cmd_settings, cmd_settings_t, pre_settings_t, post_settings_t
    use seakeeping_error_handling, only: file_not_found_error
    implicit none

    type(region_t) :: region
    class(cmd_settings_t), allocatable :: cmd_settings  !! Command line settings <br>
                                                        !! 命令行设置
    character(:), allocatable :: manifest               !! Manifest <br>
                                                        !! 清单文件

    ! ----------------------- 启动 To-SPH 程序 ----------------------- !
    call tic()
    call set_terminal()
    call get_cmd_settings(cmd_settings)
    call set_log_file(cmd_settings%working_dir.join.'to-sph.log')
    call stdlog%log_information('启动 To-SPH')

    ! ------------------------- 运行核心程序 ------------------------- !
    manifest = cmd_settings%working_dir.join."sph.toml"
    if (is_exist(manifest)) then
        ! call get_project_configuration(manifest, project)
    else
        call file_not_found_error(manifest)
    end if

    print '(a)', '*** 粒子模型前后处理程序 To-SPH v0.4.1'

    select type (cmd_settings)
    type is (pre_settings_t)
        call cmd_pre(cmd_settings, region)
    type is (post_settings_t)
        call cmd_post(cmd_settings, region)
    end select

    ! --------------------------- 运行结束 -------------------------- !
    call stdlog%log_information('结束 To-SPH')
    print '(a)', '*** To-SPH 正常结束'
    call toc(now())

end program main
