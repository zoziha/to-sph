module test_sph_load_lua

    use sph_load_lua, only: load_lua_script
    use seakeeping_kinds, only: rk
    use testdrive, only: new_unittest, unittest_type, error_type, check
    use sph_region_type, only: region_t
    implicit none
    private

    public :: collect_load_lua

contains

    subroutine collect_load_lua(test_suite)
        type(unittest_type), allocatable, intent(out) :: test_suite(:)

        test_suite = [ &
                     new_unittest("sub: load_lua_script", test_load_lua_script) &
                     ]

    end subroutine collect_load_lua
    
    subroutine test_load_lua_script(error)
        type(error_type), allocatable, intent(out) :: error
        type(region_t) :: region
        call load_lua_script('test/test.lua', region)
        call check(error, region%nreal, 1600)
        if (allocated(error)) return
        call check(error, region%nvirt, 0)
        if (allocated(error)) return
        call check(error, region%hsml, 0.000025_rk)
        if (allocated(error)) return
        call check(error, region%itype(1), 2)
        if (allocated(error)) return
        call check(error, region%vel(1, 1), 0.0_rk)
        if (allocated(error)) return
        call check(error, region%u(1), 357.1_rk)
        if (allocated(error)) return
        call check(error, region%rho(1), 1004.9375000000000_rk)
        if (allocated(error)) return
        call check(error, region%p(1), 9.6775000000000002_rk)
        if (allocated(error)) return
        call check(error, region%loc(1, 1), 1.2500000000000001E-005_rk)
    end subroutine test_load_lua_script

end module test_sph_load_lua
