program tester

    use, intrinsic :: iso_fortran_env, only: error_unit
    use seakeeping_logger, only: stdlog => global_logger
    use testdrive, only: run_testsuite, new_testsuite, testsuite_type
    use test_sph_load_lua, only: collect_load_lua
    implicit none
    integer stat, is
    type(testsuite_type), allocatable :: test_suites(:)
    character(*), parameter :: fmt = "('#', *(1x, a))"

    stat = 0
    call stdlog%add_log_file('test/.stdlog.log')
    call stdlog%log_information('Starting TEST')
    test_suites = [ &
                  new_testsuite("mod: test_sph_load_lua", collect_load_lua) &
                  ]

    do is = 1, size(test_suites)
        write (error_unit, fmt) "Testing:", test_suites(is)%name
        call run_testsuite(test_suites(is)%collect, error_unit, stat)
    end do

    if (stat > 0) then
        write (error_unit, '(i0, 1x, a)') stat, "test(s) failed!"
        error stop
    end if

end program tester
