title: Lua 接口规范
---

# Lua 接口规范

[TOC]

## Lua 脚本名

Lua 的脚本名规定为 `pif.lua`。

## Lua 函数接口

presph 采用 Lua 方法布置初始粒子信息，函数名规定为 `region`，意为“计算粒子域”。
```lua
function region()

    -- ...
    
    return x, xv, mass, rho, p, u, itype, dx, nreal, nvirt
end
```

函数没有输入值，只有 10 个输出值。分别是：

- `x`: 粒子位置，类型为 `table`，`x[][]`，第一个索引为维度，第二个索引为粒子序号。
- `xv`：粒子速度，类型为 `table`，`xv[][]`，第一个索引为维度，第二个索引为粒子序号。
- `mass`：粒子质量，类型为 `table`，`mass[]`，第一个索引为粒子序号。
- `rho`：粒子密度，类型为 `table`，`rho[]`，第一个索引为粒子序号。
- `p`：粒子压强，类型为 `table`，`p[]`，第一个索引为粒子序号。
- `u`：粒子内能，类型为 `table`，`u[]`，第一个索引为粒子序号。
- `itype`：粒子类型，类型为 `table`，`itype[]`，第一个索引为粒子序号。
- `dx`：光滑长度，类型为 `number`。
- `nreal`：实粒子数，类型为 `number`。
- `nvirt`：虚拟粒子数，类型为 `number`。
