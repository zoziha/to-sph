module sph_io_env

    use seakeeping_kinds, only: rk
    implicit none

    !> 序列：0 ~ ntotal
    integer, allocatable :: idx(:)
    !> 序列：1
    integer(1), allocatable :: ones(:)

end module sph_io_env
