!> author: 左志华
!> date: 2022-07-30
!>
!> Logger <br>
!> 日志
module sph_logger_m

    use seakeeping_logger, only: stdlog => global_logger
    use seakeeping_filesystem, only: operator(.join.), is_exist, is_windows, mkdir
    implicit none
    private

    public :: stdlog, set_log_file

contains

    !> Set log file <br>
    !> 设置日志文件
    subroutine set_log_file(file)
        character(*), intent(in) :: file        !! File name of the log file <br>
                                                !! 日志文件名
        integer :: log_unit

        open (newunit=log_unit, file=file)
        call stdlog%add_log_unit(log_unit)

    end subroutine set_log_file

end module sph_logger_m
