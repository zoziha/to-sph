!> author: 左志华
!> date: 2022-07-20
!>
!> Region <br>
!> 计算域派生类型
module sph_region_type

    use seakeeping_kinds, only: rk
    implicit none

    !> Region <br>
    !> 域
    type region_t
        integer :: dim                      !! Dimension of the region <br>
                                            !! 维度
        integer :: nreal                    !! Number of real particles <br>
                                            !! 实粒子数
        integer :: nvirt                    !! Number of virtual particles <br>
                                            !! 虚粒子数
        integer :: ntotal                   !! Total number of particles <br>
                                            !! 粒子总数
        real(rk) :: hsml                    !! Smoothing length <br>
                                            !! 光滑长度
        real(rk), allocatable :: loc(:, :)  !! Location of the particles <br>
                                            !! 粒子坐标
        real(rk), allocatable :: vel(:, :)  !! Velocity of the particles <br>
                                            !! 粒子速度
        real(rk), allocatable :: mass(:)    !! Mass of the particles <br>
                                            !! 粒子质量
        real(rk), allocatable :: rho(:)     !! Density of the particles <br>
                                            !! 粒子密度
        real(rk), allocatable :: p(:)       !! Pressure of the particles <br>
                                            !! 粒子压强
        real(rk), allocatable :: u(:)       !! Internal energy of the particles <br>
                                            !! 粒子内能
        integer, allocatable :: itype(:)    !! Type of the particles <br>
                                            !! 粒子类型
    end type region_t

end module sph_region_type
